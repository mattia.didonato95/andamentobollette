# in this file we extract from a file.xls all data and put them into a json file

import pandas as pd
import json

# read the file
df = pd.read_excel('../../input/Lista_Movimenti.xls')

# read from 15th row the column name 
df = pd.read_excel('../../input/Lista_Movimenti.xls', skiprows=15)

# use first row as column name
df.columns = df.iloc[0]

# drop the first row
df = df.drop(df.index[0])

# drop last 3 rows
df = df.drop(df.tail(3).index)

# drop first column
df = df.drop(df.columns[0], axis=1)

# convert df to a json array and save it into a file
df.to_json('../../output/movimenti2023.json', orient='records')


# for each json object in the array we add a new field called "id" with a progressive number
with open('../../output/movimenti2023.json', 'r') as f:
    data = json.load(f)
    i = 0
    for item in data:
        item['id'] = i
        i += 1

# save the new json array into a file
with open('../../output/movimenti2023.json', 'w') as f:
    json.dump(data, f, indent=4)

# for each json object in the array split the field "Data Contabile" into 3 fields: "giorno", "mese", "anno" and put "mese" 
with open('../../output/movimenti2023.json', 'r') as f:
    data = json.load(f)
    for item in data:
        item['giorno'] = item['Data operazione'].split(' ')[0]
        item['mese'] = item['Data operazione'].split(' ')[1]
        # convert the month into numeric value
        if item['mese'] == 'gennaio':
            item['mese'] = '01'
        elif item['mese'] == 'febbraio':
            item['mese'] = '02'
        elif item['mese'] == 'marzo':
            item['mese'] = '03'
        elif item['mese'] == 'aprile':
            item['mese'] = '04'
        elif item['mese'] == 'maggio':
            item['mese'] = '05'
        elif item['mese'] == 'giugno':
            item['mese'] = '06'
        elif item['mese'] == 'luglio':
            item['mese'] = '07'
        elif item['mese'] == 'agosto':
            item['mese'] = '08'
        elif item['mese'] == 'settembre':
            item['mese'] = '09'
        elif item['mese'] == 'ottobre':
            item['mese'] = '10'
        elif item['mese'] == 'novembre':
            item['mese'] = '11'
        elif item['mese'] == 'dicembre':
            item['mese'] = '12'
        item['anno'] = item['Data operazione'].split(' ')[2]

# save the new json array into a file
with open('../../output/movimenti2023.json', 'w') as f:
    json.dump(data, f, indent=4)


# split the json array if the bank movement is an income or an expense and create a json object with two arrays: "entrata" and "uscita"
with open('../../output/movimenti2023.json', 'r') as f:
    data = json.load(f)
    entrata = []
    uscita = []
    for item in data:
        if item['Entrate'] is not None:
            entrata.append(item)
        else:
            uscita.append(item)
    json_data = {
        "entrata": entrata,
        "uscita": uscita
    }

# save the new json object into a file   ../frontend/src/pages/report.json
with open('../../output/movimenti2023.json', 'w') as f:
    json.dump(json_data, f, indent=4)

# convert all json into uscita array into a positive number
with open('../../output/movimenti2023.json', 'r') as f:
    data = json.load(f)
    for item in data['uscita']:
        item['Uscite'] = item['Uscite'] * -1


# save the new json object into a file   ../frontend/src/pages/report.json
with open('../../output/movimenti2023.json', 'w') as f:
    json.dump(data, f, indent=4)


# convert all json into uscita array into a positive number
with open('../../output/movimenti2023.json', 'r') as f:
    data = json.load(f)

somme_mensili = {}
# Creiamo una lista di tutti i mesi dell'anno
tutti_i_mesi = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]

# Iteriamo attraverso le transazioni di uscita
for transazione in data["uscita"]:
    mese = transazione["mese"]

    # Verifichiamo se il mese è già nel dizionario, altrimenti inizializziamo a 0
    somme_mensili.setdefault(mese, 0)

    # Aggiungiamo l'importo alla somma del mese corrente
    somme_mensili[mese] += transazione["Uscite"] if transazione["Uscite"] is not None else 0

# Assicuriamoci che tutti i mesi siano rappresentati nel risultato
risultato_finale_uscita = [{"mese": mese, "somma_uscite": somme_mensili.get(mese, 0)} for mese in tutti_i_mesi]

# Ripetiamo lo stesso processo per le transazioni di entrata
somme_mensili = {}
for transazione in data["entrata"]:
    mese = transazione["mese"]

    somme_mensili.setdefault(mese, 0)
    somme_mensili[mese] += transazione["Entrate"] if transazione["Entrate"] is not None else 0

risultato_finale_entrata = [{"mese": mese, "somma_entrate": somme_mensili.get(mese, 0)} for mese in tutti_i_mesi]

# Creiamo un nuovo oggetto JSON con due array: "entrata" e "uscita"
final = {
    "uscita": risultato_finale_uscita,
    "entrata": risultato_finale_entrata
}




# save the new json object into a file
with open('../../output/movimenti2023.json', 'w') as f:
    json.dump(final, f, indent=4)

with open('../../frontend/src/pages/movimenti2023.json', 'w') as f:
    json.dump(final, f, indent=4)
