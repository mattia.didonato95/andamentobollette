#include "util.h"

using namespace std;

vector<risultatoMensile> createReport(int year)
{
    // ESTRAZIONE DATI DAI FILE
    ifstream inFile;
    string line;
    string files[4] = {"acqua.txt", "tari.txt", "internet.txt", "lucegas.txt"};
    inputDataList inputListRead;
    ofstream debug;

    // prendo la data odierna
    auto now = std::chrono::system_clock::now();
    auto in_time_t = std::chrono::system_clock::to_time_t(now);
    std::stringstream ss;
    ss << std::put_time(std::localtime(&in_time_t), "%d/%m/%Y %X");

    debug.open("../output/debug.txt", std::ios_base::app);
    debug << "================ " << ss.str() << " =================" << endl;

    // convert int to string
    stringstream ssYear;
    ssYear << year;
    string yearString = ssYear.str();

    for (int i = 0; i < 4; i++)
    {
        inFile.open("../input/" + yearString + "/" + files[i]);

        while (getline(inFile, line))
        {
            line.length() == 25 ? line.erase(line.length() - 1) : ""; // elimino il carattere \n
            inputDataLine dataRead;
            dataRead.lineRead = line;
            dataRead.fileSource = files[i];

            debug << dataRead.lineRead << " " << dataRead.fileSource << endl; // debug

            switch (i)
            {
            case 0:
                inputListRead.acqua.push_back(dataRead);
                break;
            case 1:
                inputListRead.tari.push_back(dataRead);
                break;
            case 2:
                inputListRead.internet.push_back(dataRead);
                break;
            case 3:
                inputListRead.lucegas.push_back(dataRead);
                break;
            default:
                break;
            };
        };

        inFile.close();
    };

    debug << "======================================================" << endl;
    debug.close();
    // DATI ESTRATTI E INSERITI IN inputRead{}

    //------------------------------------------------------------------------

    vector<risultatoMensile> anno = stampaRisultato(inputListRead);

    return anno;
}

vector<risultatoMensile> stampaRisultato(inputDataList inputDatas)
{
    // inizializzazione strutture
    vector<risultatoMensile> anno(12);
    // risultatoMensile anno[12];
    string mesi[12] = {"GEN", "FEB", "MAR", "APR", "MAG", "GIU", "LUG", "AGO", "SET", "OTT", "NOV", "DIC"};
    for (int i = 0; i < 12; i++)
    {
        anno[i].importo = 0;
        anno[i].mese = mesi[i];
        anno[i].acqua = 0;
        anno[i].tari = 0;
        anno[i].internet = 0;
        anno[i].lucegas = 0;
    }

    for (int i = 0; i < inputDatas.acqua.size(); i++)
    {
        inputDatas.acqua[i].lineStringSplit = formatta(inputDatas.acqua[i].lineRead);                               // prendo prezzo, from e to
        inputDatas.acqua[i].lineIntSplit = formattaInt(inputDatas.acqua[i].lineStringSplit);                        // prendo g1,g2,m2,m1 interi
        inputDatas.acqua[i].infoCompentenza.mesiCompetenza = trovaMesiCompetenza(inputDatas.acqua[i].lineIntSplit); // trovo i mesi di competenza
        inputDatas.acqua[i].infoCompentenza.prezzoMensile = prezzoMensile(inputDatas.acqua[i]);                     // trovo il prezzo mensile
    }
    for (int i = 0; i < inputDatas.tari.size(); i++)
    {
        inputDatas.tari[i].lineStringSplit = formatta(inputDatas.tari[i].lineRead);                               // prendo prezzo, from e to
        inputDatas.tari[i].lineIntSplit = formattaInt(inputDatas.tari[i].lineStringSplit);                        // prendo g1,g2,m2,m1 interi
        inputDatas.tari[i].infoCompentenza.mesiCompetenza = trovaMesiCompetenza(inputDatas.tari[i].lineIntSplit); // trovo i mesi di competenza
        inputDatas.tari[i].infoCompentenza.prezzoMensile = prezzoMensile(inputDatas.tari[i]);                     // trovo il prezzo mensile
    }
    for (int i = 0; i < inputDatas.internet.size(); i++)
    {
        inputDatas.internet[i].lineStringSplit = formatta(inputDatas.internet[i].lineRead);                               // prendo prezzo, from e to
        inputDatas.internet[i].lineIntSplit = formattaInt(inputDatas.internet[i].lineStringSplit);                        // prendo g1,g2,m2,m1 interi
        inputDatas.internet[i].infoCompentenza.mesiCompetenza = trovaMesiCompetenza(inputDatas.internet[i].lineIntSplit); // trovo i mesi di competenza
        inputDatas.internet[i].infoCompentenza.prezzoMensile = prezzoMensile(inputDatas.internet[i]);                     // trovo il prezzo mensile
    }
    for (int i = 0; i < inputDatas.lucegas.size(); i++)
    {
        inputDatas.lucegas[i].lineStringSplit = formatta(inputDatas.lucegas[i].lineRead);                               // prendo prezzo, from e to
        inputDatas.lucegas[i].lineIntSplit = formattaInt(inputDatas.lucegas[i].lineStringSplit);                        // prendo g1,g2,m2,m1 interi
        inputDatas.lucegas[i].infoCompentenza.mesiCompetenza = trovaMesiCompetenza(inputDatas.lucegas[i].lineIntSplit); // trovo i mesi di competenza
        inputDatas.lucegas[i].infoCompentenza.prezzoMensile = prezzoMensile(inputDatas.lucegas[i]);                     // trovo il prezzo mensile
    }

    for (int i = 0; i < inputDatas.acqua.size(); i++)
    {
        for (int j = 0; j < inputDatas.acqua[i].infoCompentenza.mesiCompetenza.size(); j++)
        {
            int k = inputDatas.acqua[i].infoCompentenza.mesiCompetenza[j];
            anno[k - 1].importo += inputDatas.acqua[i].infoCompentenza.prezzoMensile;
            anno[k - 1].acqua += inputDatas.acqua[i].infoCompentenza.prezzoMensile;
        };
    };
    for (int i = 0; i < inputDatas.tari.size(); i++)
    {
        for (int j = 0; j < inputDatas.tari[i].infoCompentenza.mesiCompetenza.size(); j++)
        {
            int k = inputDatas.tari[i].infoCompentenza.mesiCompetenza[j];
            anno[k - 1].importo += inputDatas.tari[i].infoCompentenza.prezzoMensile;
            anno[k - 1].tari += inputDatas.tari[i].infoCompentenza.prezzoMensile;
        };
    };
    for (int i = 0; i < inputDatas.internet.size(); i++)
    {
        for (int j = 0; j < inputDatas.internet[i].infoCompentenza.mesiCompetenza.size(); j++)
        {
            int k = inputDatas.internet[i].infoCompentenza.mesiCompetenza[j];
            anno[k - 1].importo += inputDatas.internet[i].infoCompentenza.prezzoMensile;
            anno[k - 1].internet += inputDatas.internet[i].infoCompentenza.prezzoMensile;
        };
    };
    for (int i = 0; i < inputDatas.lucegas.size(); i++)
    {
        for (int j = 0; j < inputDatas.lucegas[i].infoCompentenza.mesiCompetenza.size(); j++)
        {
            int k = inputDatas.lucegas[i].infoCompentenza.mesiCompetenza[j];
            anno[k - 1].importo += inputDatas.lucegas[i].infoCompentenza.prezzoMensile;
            anno[k - 1].lucegas += inputDatas.lucegas[i].infoCompentenza.prezzoMensile;
        };
    };

    // prendo la data odierna
    auto now = std::chrono::system_clock::now();
    auto in_time_t = std::chrono::system_clock::to_time_t(now);
    std::stringstream ss;
    ss << std::put_time(std::localtime(&in_time_t), "%d/%m/%Y %X");

    ofstream out;
    out.open("../output/risultati.txt", std::ios_base::out);
    out << "================ " << ss.str() << " =================" << endl;

    for (int i = 0; i < 12; i++)
    {
        out << anno[i].mese << " - " << anno[i].importo
            << " - [acqua: " << anno[i].acqua
            << "] - [tari: " << anno[i].tari
            << "] - [internet: " << anno[i].internet
            << "] - [lucegas: " << anno[i].lucegas
            << "]" << endl;
    }

    out << "======================================================" << endl;
    out.close();

    return anno;
};

split formatta(string line)
{
    // estrae giorno mese per il periodo di riferimento del pagamento
    // viene chiamata dalla funzione Formattatore()
    // restituisce un oggetto diffMesi

    split elem;

    // conversione string to char[] per usare la strtok
    int n = line.length();
    char charLine[n + 1];
    strcpy(charLine, line.c_str());

    // splitto line in prezz, from e to
    char *p, *fr, *t;
    p = strtok(charLine, "-");
    fr = strtok(NULL, "-");
    t = strtok(NULL, "-");

    // compilo elem
    elem.prezzo = p;
    elem.from = fr;
    elem.to = t;

    return elem;
}

spltInteger formattaInt(split elem)
{
    char *mese1, *mese2, *gg1, *gg2;

    // spezzo from
    int n = elem.from.length();
    char fromline[n + 1];
    strcpy(fromline, elem.from.c_str());
    gg1 = strtok(fromline, "/");
    mese1 = strtok(NULL, "/");
    // spezzo to
    int m = elem.to.length();
    char toline[m + 1];
    strcpy(toline, elem.to.c_str());
    gg2 = strtok(toline, "/");
    mese2 = strtok(NULL, "/");

    // converto tutto in interi
    spltInteger splitInt;
    splitInt.mese1 = atoi(mese1);
    splitInt.giorno1 = atoi(gg1);
    splitInt.mese2 = atoi(mese2);
    splitInt.giorno2 = atoi(gg2);
    // cout << m1 << " " << g1 << " " << m2 << " " << g2 << " " << endl;

    return splitInt;
}

vector<int> trovaMesiCompetenza(spltInteger elem)
{
    int mese1 = elem.mese1;
    int mese2 = elem.mese2;

    int differenza = mese2 - mese1 + 1; // mese1 e mese2 inclusi
    if (elem.giorno1 > 15)
    {
        differenza -= 1;
        mese1 += 1; // tolgo il mese1
    }
    if (elem.giorno2 < 15)
    {
        differenza -= 1;
        mese2 -= 1; // tolgo il mese2
    }
    vector<int> mesiCompetenza;
    for (int i = mese1; i <= mese2; i++)
    {
        mesiCompetenza.push_back(i);
    }

    // cout << "differenza:" << differenza << " mese1: " << mese1 << " mese2: " << mese2 << endl;

    return mesiCompetenza;
}

int prezzoMensile(inputDataLine inputLine)
{
    int quantiMesi = inputLine.infoCompentenza.mesiCompetenza.size();
    int prezzoMensile = stoi(inputLine.lineStringSplit.prezzo) / quantiMesi;
    return prezzoMensile;
}

int generateUniqueID(std::mt19937 &gen, std::set<int> &generatedIDs, std::uniform_int_distribution<> &dis)
{
    int uniqueID;

    do
    {
        uniqueID = dis(gen);
    } while (generatedIDs.count(uniqueID) > 0);

    generatedIDs.insert(uniqueID);
    return uniqueID;
}

int contaMesiConImportoDiversoDaZero(vector<risultatoMensile> anno) {
    int count = 0;
    
    for (const auto& risultato : anno) {
        if (risultato.importo != 0) {
            count++;
        }
    }
    
    return count;
}


void createJSONreport()
{

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(1, std::numeric_limits<int>::max());

    // Set per mantenere traccia degli ID già generati
    std::set<int> generatedIDs;

    json jsonReport = {
        {"data", {{"2021", {{"numero_mesi", 12}, {"acqua", {{"totale", 0}, {"list", json::array()}}}, {"tari", {{"totale", 0}, {"list", json::array()}}}, {"internet", {{"totale", 0}, {"list", json::array()}}}, {"lucegas", {{"totale", 0}, {"list", json::array()}}}}}, {"2022", {{"numero_mesi", 12}, {"acqua", {{"totale", 0}, {"list", json::array()}}}, {"tari", {{"totale", 0}, {"list", json::array()}}}, {"internet", {{"totale", 0}, {"list", json::array()}}}, {"lucegas", {{"totale", 0}, {"list", json::array()}}}}}, {"2023", {{"numero_mesi", 12}, {"acqua", {{"totale", 0}, {"list", json::array()}}}, {"tari", {{"totale", 0}, {"list", json::array()}}}, {"internet", {{"totale", 0}, {"list", json::array()}}}, {"lucegas", {{"totale", 0}, {"list", json::array()}}}}}, {"2024", {{"numero_mesi", 12}, {"acqua", {{"totale", 0}, {"list", json::array()}}}, {"tari", {{"totale", 0}, {"list", json::array()}}}, {"internet", {{"totale", 0}, {"list", json::array()}}}, {"lucegas", {{"totale", 0}, {"list", json::array()}}}}}}}};

    vector<int>
        yearList = {2021, 2022, 2023, 2024};

    for (int year : yearList)
    {
        vector<risultatoMensile> anno = createReport(year);
        int numeroMesi = static_cast<int>(anno.size());
        cout << "stampa anno: " << year << endl;

        int totAcqua = 0;
        int totTari = 0;
        int totInternet = 0;
        int totLucegas = 0;
        string yearString = to_string(year);

       jsonReport["data"][yearString]["numero_mesi"] = contaMesiConImportoDiversoDaZero(anno);
       
        for (int i = 0; i < 12; i++)
        {
            totAcqua += anno[i].acqua;
            totTari += anno[i].tari;
            totInternet += anno[i].internet;
            totLucegas += anno[i].lucegas;

            json acqua = {{"ID", generateUniqueID(gen, generatedIDs, dis)}, {"mese", anno[i].mese}, {"totMese", anno[i].importo}, {"importo", anno[i].acqua}, {"ente", "acqua"}, {"anno", year}};
            jsonReport["data"][yearString]["acqua"]["list"].push_back(acqua);

            json tari = {{"ID", generateUniqueID(gen, generatedIDs, dis)}, {"mese", anno[i].mese}, {"totMese", anno[i].importo}, {"importo", anno[i].tari}, {"ente", "tari"}, {"anno", year}};
            jsonReport["data"][yearString]["tari"]["list"].push_back(tari);

            json internet = {{"ID", generateUniqueID(gen, generatedIDs, dis)}, {"mese", anno[i].mese}, {"totMese", anno[i].importo}, {"importo", anno[i].internet}, {"ente", "internet"}, {"anno", year}};
            jsonReport["data"][yearString]["internet"]["list"].push_back(internet);

            json lucegas = {{"ID", generateUniqueID(gen, generatedIDs, dis)}, {"mese", anno[i].mese}, {"totMese", anno[i].importo}, {"importo", anno[i].lucegas}, {"ente", "lucegas"}, {"anno", year}};
            jsonReport["data"][yearString]["lucegas"]["list"].push_back(lucegas);
        }

        jsonReport["data"][yearString]["acqua"]["totale"] = totAcqua;
        jsonReport["data"][yearString]["tari"]["totale"] = totTari;
        jsonReport["data"][yearString]["internet"]["totale"] = totInternet;
        jsonReport["data"][yearString]["lucegas"]["totale"] = totLucegas;
    }

    std::ofstream o("../output/report.json");
    o << std::setw(4) << jsonReport << std::endl;

    std::ofstream o2("../frontend/src/pages/report.json");
    o2 << std::setw(4) << jsonReport << std::endl;

    return;
}