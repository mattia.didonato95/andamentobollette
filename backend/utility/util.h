#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstring>
#include <vector>
#include <chrono>
#include <fstream>
#include <nlohmann/json.hpp>
#include <limits>
#include <random>
#include <set>


using namespace std;
using json = nlohmann::json;

struct competenza
{
    vector<int> mesiCompetenza;
    int prezzoMensile;
};
struct spltInteger
{
    int mese1;
    int giorno1;
    int mese2;
    int giorno2;
};
struct split
{
    string prezzo;
    string from;
    string to;
};
struct inputDataLine
{
    string lineRead;
    string fileSource;
    split lineStringSplit;
    spltInteger lineIntSplit;
    competenza infoCompentenza;
};
struct inputDataList
{
    vector<inputDataLine> acqua;
    vector<inputDataLine> tari;
    vector<inputDataLine> internet;
    vector<inputDataLine> lucegas;
};
struct risultatoMensile
{
    int importo;
    string mese;
    int acqua, tari, internet, lucegas;
};

vector<risultatoMensile> createReport(int year);

vector<risultatoMensile> stampaRisultato(inputDataList);

split formatta(string);

spltInteger formattaInt(split);

vector<int> trovaMesiCompetenza(spltInteger);

int prezzoMensile(inputDataLine);

void createJSONreport();

int contaMesiConImportoDiversoDaZero(vector<risultatoMensile>);