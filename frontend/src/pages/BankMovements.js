import { Helmet } from 'react-helmet-async';
import { faker } from '@faker-js/faker';
// @mui
import { useTheme } from '@mui/material/styles';
import { Grid, Container, Typography } from '@mui/material';
// components
import Iconify from '../components/iconify';
// sections
import {
  AppTasks,
  AppNewsUpdate,
  AppOrderTimeline,
  AppCurrentVisits,
  AppWebsiteVisits,
  AppTrafficBySite,
  AppWidgetSummary,
  AppCurrentSubject,
  AppConversionRates,
} from '../sections/@dashboard/app';
import movements from './movimenti2023.json';

// ----------------------------------------------------------------------

export default function BankMovement() {
  const theme = useTheme();


  console.log(movements.entrata.map((elem) => elem?.Entrate));

  return (
    <>
      <Helmet>
        <title> BANK MOVEMENTS </title>
      </Helmet>

      <Container maxWidth="xl">
        <Grid container spacing={3}>

          <Grid item xs={12} md={12} lg={12}>
            <AppWebsiteVisits
              title="Month bank movements in 2023"
              chartLabels={[
                '01/01/2023',
                '02/01/2023',
                '03/01/2023',
                '04/01/2023',
                '05/01/2023',
                '06/01/2023',
                '07/01/2023',
                '08/01/2023',
                '09/01/2023',
                '10/01/2023',
                '11/01/2023',
                '12/01/2023',
              ]}
              // chartLabels={['GEN', 'FEB', 'MAR', 'APR', 'MAG', 'GIU', 'LUG', 'AGO', 'SET', 'OTT', 'NOV', 'DIC']}
              chartData={[
                {
                  name: 'entrate',
                  type: 'area',
                  fill: 'gradient',
                  data: movements.entrata.map((elem) => elem?.somma_entrate),
                  // data: [23, 11, 22, 27, 13, 22, 37, 21, 44, 22, 30],
                },
                {
                  name: 'uscite',
                  type: 'area',
                  fill: 'gradient',
                  data: movements.uscita.map((elem) => elem?.somma_uscite),
                  // data: [30, 25, 36, 30, 45, 35, 64, 52, 59, 36, 39],
                },
              ]}
            />
          </Grid>

        </Grid>
      </Container>
    </>
  );
}
