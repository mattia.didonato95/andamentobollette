import { Helmet } from 'react-helmet-async';
import { faker } from '@faker-js/faker';
// @mui
import { useTheme } from '@mui/material/styles';
import { Grid, Container, Typography } from '@mui/material';
// components
import Iconify from '../components/iconify';
// sections
import {
  AppTasks,
  AppNewsUpdate,
  AppOrderTimeline,
  AppCurrentVisits,
  AppWebsiteVisits,
  AppTrafficBySite,
  AppWidgetSummary,
  AppCurrentSubject,
  AppConversionRates,
} from '../sections/@dashboard/app';
import data from './report.json';

// ----------------------------------------------------------------------

export default function DashboardAppPage() {
  const theme = useTheme();

  return (
    <>
      <Helmet>
        <title> Dashboard | Minimal UI </title>
      </Helmet>

      <Container maxWidth="xl">
        <Grid container spacing={3}>
          {/*  <Grid item xs={12} sm={6} md={3}>
            <AppWidgetSummary title="Weekly Sale s" total={714000} icon={'ant-design:android-filled'} />
          </Grid>

          <Grid item xs={12} sm={6} md={3}>
            <AppWidgetSummary title="New Users" total={1352831} color="info" icon={'ant-design:apple-filled'} />
          </Grid>

          <Grid item xs={12} sm={6} md={3}>
            <AppWidgetSummary title="Item Orders" total={1723315} color="warning" icon={'ant-design:windows-filled'} />
          </Grid>

          <Grid item xs={12} sm={6} md={3}>
            <AppWidgetSummary title="Bug Reports" total={234} color="error" icon={'ant-design:bug-filled'} />
          </Grid> */}

          <Grid item xs={12} md={12} lg={12}>
            <AppWebsiteVisits
              title="Month costs 2021"
              chartLabels={[
                '01/01/2021',
                '02/01/2021',
                '03/01/2021',
                '04/01/2021',
                '05/01/2021',
                '06/01/2021',
                '07/01/2021',
                '08/01/2021',
                '09/01/2021',
                '10/01/2021',
                '11/01/2021',
                '12/01/2021',
              ]}
              // chartLabels={['GEN', 'FEB', 'MAR', 'APR', 'MAG', 'GIU', 'LUG', 'AGO', 'SET', 'OTT', 'NOV', 'DIC']}
              chartData={[
                {
                  name: 'acqua',
                  type: 'area',
                  fill: 'gradient',
                  data: data?.data['2021'].acqua?.list.map((elem) => elem?.acqua),
                  // data: [23, 11, 22, 27, 13, 22, 37, 21, 44, 22, 30],
                },
                {
                  name: 'tari',
                  type: 'column',
                  fill: 'gradient',
                  data: data?.data['2021'].tari?.list.map((elem) => elem?.tari),
                  // data: [44, 55, 41, 67, 22, 43, 21, 41, 56, 27, 43],
                },
                {
                  name: 'internet',
                  type: 'column',
                  fill: 'gradient',
                  data: data?.data['2021'].internet?.list.map((elem) => elem?.internet),
                  // data: [30, 25, 36, 30, 45, 35, 64, 52, 59, 36, 39],
                },
                {
                  name: 'lucegas',
                  type: 'area',
                  fill: 'gradient',
                  data: data?.data['2021'].lucegas?.list.map((elem) => elem?.lucegas),
                  // data: [30, 25, 36, 30, 45, 35, 64, 52, 59, 36, 39],
                },
              ]}
            />
          </Grid>

          <Grid item xs={12} md={12} lg={12}>
            ?.list
            <AppWebsiteVisits
              title="Month costs 2022"
              chartLabels={[
                '01/01/2022',
                '02/01/2022',
                '03/01/2022',
                '04/01/2022',
                '05/01/2022',
                '06/01/2022',
                '07/01/2022',
                '08/01/2022',
                '09/01/2022',
                '10/01/2022',
                '11/01/2022',
                '12/01/2022',
              ]}
              // chartLabels={['GEN', 'FEB', 'MAR', 'APR', 'MAG', 'GIU', 'LUG', 'AGO', 'SET', 'OTT', 'NOV', 'DIC']}
              chartData={[
                {
                  name: 'acqua',
                  type: 'area',
                  fill: 'gradient',
                  data: data?.data['2022'].acqua?.list.map((elem) => elem?.acqua),
                  // data: [23, 11, 22, 27, 13, 22, 37, 21, 44, 22, 30],
                },
                {
                  name: 'tari',
                  type: 'column',
                  fill: 'gradient',
                  data: data?.data['2022'].tari?.list.map((elem) => elem?.tari),
                  // data: [44, 55, 41, 67, 22, 43, 21, 41, 56, 27, 43],
                },
                {
                  name: 'internet',
                  type: 'column',
                  fill: 'gradient',
                  data: data?.data['2022'].internet?.list.map((elem) => elem?.internet),
                  // data: [30, 25, 36, 30, 45, 35, 64, 52, 59, 36, 39],
                },
                {
                  name: 'lucegas',
                  type: 'area',
                  fill: 'gradient',
                  data: data?.data['2022'].lucegas?.list.map((elem) => elem?.lucegas),
                  // data: [30, 25, 36, 30, 45, 35, 64, 52, 59, 36, 39],
                },
              ]}
            />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <AppCurrentVisits
              title="2021 distribution"
              chartData={[
                { label: 'acqua', value: data?.data['2021'].acqua?.totale },
                { label: 'tari', value: data?.data['2021'].tari?.totale },
                { label: 'internet', value: data?.data['2021'].internet?.totale },
                { label: 'lucegas', value: data?.data['2021'].lucegas?.totale },
              ]}
              chartColors={[
                theme.palette.primary.main,
                theme.palette.info.main,
                theme.palette.warning.main,
                theme.palette.error.main,
              ]}
            />
          </Grid>

          <Grid item xs={12} md={6} lg={8}>
            <AppConversionRates
              title="2021 month distribution"
              chartData={[
                { label: 'GEN', value: data.data['2021']?.acqua.list[0]?.totMese },
                { label: 'FEB', value: data.data['2021']?.acqua.list[1]?.totMese },
                { label: 'MAR', value: data.data['2021']?.acqua.list[2]?.totMese },
                { label: 'APR', value: data.data['2021']?.acqua.list[3]?.totMese },
                { label: 'MAG', value: data.data['2021']?.acqua.list[4]?.totMese },
                { label: 'GIU', value: data.data['2021']?.acqua.list[5]?.totMese },
                { label: 'LUG', value: data.data['2021']?.acqua.list[6]?.totMese },
                { label: 'AGO', value: data.data['2021']?.acqua.list[7]?.totMese },
                { label: 'SET', value: data.data['2021']?.acqua.list[8]?.totMese },
                { label: 'OTT', value: data.data['2021']?.acqua.list[9]?.totMese },
                { label: 'NOV', value: data.data['2021']?.acqua.list[10]?.totMese },
                { label: 'DIC', value: data.data['2021']?.acqua.list[11]?.totMese },
              ]}
            />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <AppCurrentVisits
              title="2022 distribution"
              chartData={[
                { label: 'acqua', value: data?.data['2022'].acqua?.totale },
                { label: 'tari', value: data?.data['2022'].tari?.totale },
                { label: 'internet', value: data?.data['2022'].internet?.totale },
                { label: 'lucegas', value: data?.data['2022'].lucegas?.totale },
              ]}
              chartColors={[
                theme.palette.primary.main,
                theme.palette.info.main,
                theme.palette.warning.main,
                theme.palette.error.main,
              ]}
            />
          </Grid>

          <Grid item xs={12} md={6} lg={8}>
            <AppConversionRates
              title="2022 month distribution"
              chartData={[
                { label: 'GEN', value: data.data['2022']?.acqua.list[0]?.totMese },
                { label: 'FEB', value: data.data['2022']?.acqua.list[1]?.totMese },
                { label: 'MAR', value: data.data['2022']?.acqua.list[2]?.totMese },
                { label: 'APR', value: data.data['2022']?.acqua.list[3]?.totMese },
                { label: 'MAG', value: data.data['2022']?.acqua.list[4]?.totMese },
                { label: 'GIU', value: data.data['2022']?.acqua.list[5]?.totMese },
                { label: 'LUG', value: data.data['2022']?.acqua.list[6]?.totMese },
                { label: 'AGO', value: data.data['2022']?.acqua.list[7]?.totMese },
                { label: 'SET', value: data.data['2022']?.acqua.list[8]?.totMese },
                { label: 'OTT', value: data.data['2022']?.acqua.list[9]?.totMese },
                { label: 'NOV', value: data.data['2022']?.acqua.list[10]?.totMese },
                { label: 'DIC', value: data.data['2022']?.acqua.list[11]?.totMese },
              ]}
            />
          </Grid>

          {/*  <Grid item xs={12} md={6} lg={4}>
            <AppCurrentSubject
              title="Current Subject"
              chartLabels={['English', 'History', 'Physics', 'Geography', 'Chinese', 'Math']}
              chartData={[
                { name: 'Series 1', data: [80, 50, 30, 40, 100, 20] },
                { name: 'Series 2', data: [20, 30, 40, 80, 20, 80] },
                { name: 'Series 3', data: [44, 76, 78, 13, 43, 10] },
              ]}
              chartColors={[...Array(6)].map(() => theme.palette.text.secondary)}
            />
          </Grid>

          <Grid item xs={12} md={6} lg={8}>
            <AppNewsUpdate
              title="News Update"
              list={[...Array(5)].map((_, index) => ({
                id: faker.datatype.uuid(),
                title: faker.name.jobTitle(),
                description: faker.name.jobTitle(),
                image: `/assets/images/covers/cover_${index + 1}.jpg`,
                postedAt: faker.date.recent(),
              }))}
            />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <AppOrderTimeline
              title="Order Timeline"
              list={[...Array(5)].map((_, index) => ({
                id: faker.datatype.uuid(),
                title: [
                  '1983, orders, $4220',
                  '12 Invoices have been paid',
                  'Order #37745 from September',
                  'New order placed #XF-2356',
                  'New order placed #XF-2346',
                ][index],
                type: `order${index + 1}`,
                time: faker.date.past(),
              }))}
            />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <AppTrafficBySite
              title="Traffic by Site"
              list={[
                {
                  name: 'FaceBook',
                  value: 323234,
                  icon: <Iconify icon={'eva:facebook-fill'} color="#1877F2" width={32} />,
                },
                {
                  name: 'Google',
                  value: 341212,
                  icon: <Iconify icon={'eva:google-fill'} color="#DF3E30" width={32} />,
                },
                {
                  name: 'Linkedin',
                  value: 411213,
                  icon: <Iconify icon={'eva:linkedin-fill'} color="#006097" width={32} />,
                },
                {
                  name: 'Twitter',
                  value: 443232,
                  icon: <Iconify icon={'eva:twitter-fill'} color="#1C9CEA" width={32} />,
                },
              ]}
            />
          </Grid>

          <Grid item xs={12} md={6} lg={8}>
            <AppTasks
              title="Tasks"
              list={[
                { id: '1', label: 'Create FireStone Logo' },
                { id: '2', label: 'Add SCSS and JS files if required' },
                { id: '3', label: 'Stakeholder Meeting' },
                { id: '4', label: 'Scoping & Estimations' },
                { id: '5', label: 'Sprint Showcase' },
              ]}
            />
          </Grid> */}
        </Grid>
      </Container>
    </>
  );
}
