// component
import SvgColor from '../../../components/svg-color';

// ----------------------------------------------------------------------

const icon = (name) => <SvgColor src={`/assets/icons/navbar/${name}.svg`} sx={{ width: 1, height: 1 }} />;

const navConfig = [
  /* {
    title: 'dashboard',
    path: '/dashboard/app',
    icon: icon('ic_analytics'),
  }, */
  {
    title: 'stats2021',
    path: '/dashboard/stats2021',
    icon: icon('ic_analytics'),
  },
  {
    title: 'stats2022',
    path: '/dashboard/stats2022',
    icon: icon('ic_analytics'),
  },
  {
    title: 'stats2023',
    path: '/dashboard/stats2023',
    icon: icon('ic_analytics'),
  },
  {
    title: 'stats2024',
    path: '/dashboard/stats2024',
    icon: icon('ic_analytics'),
  },
  {
    title: 'listTransaction',
    path: '/dashboard/listTransaction',
    icon: icon('ic_analytics'),
  },
  /*  {
    title: 'user',
    path: '/dashboard/user',
    icon: icon('ic_user'),
  },
  {
    title: 'product',
    path: '/dashboard/products',
    icon: icon('ic_cart'),
  },
  {
    title: 'blog',
    path: '/dashboard/blog',
    icon: icon('ic_blog'),
  },
  {
    title: 'login',
    path: '/login',
    icon: icon('ic_lock'),
  },
  {
    title: 'Not found',
    path: '/404',
    icon: icon('ic_disabled'),
  }, */
];

export default navConfig;
